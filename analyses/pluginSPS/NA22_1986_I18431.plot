BEGIN PLOT /NA22_1986_I18431/d01-x01-y01
Title=$\pi^+ p \rightarrow X$ at $p_{\mathrm{lab}}=250$ GeV/c 
XLabel=$N_{ch}$
YLabel=$P_n$
END PLOT
BEGIN PLOT /NA22_1986_I18431/d02-x01-y01
Title=$K^+ p \rightarrow X$ at $p_{\mathrm{lab}}=250$ GeV/c 
XLabel=$N_{ch}$
YLabel=$P_n$
END PLOT
BEGIN PLOT /NA22_1986_I18431/d03-x01-y01
Title=$p p \rightarrow X$ at $p_{\mathrm{lab}}=250$ GeV/c 
XLabel=$N_{ch}$
YLabel=$P_n$
END PLOT

